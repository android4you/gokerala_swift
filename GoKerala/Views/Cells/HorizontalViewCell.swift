//
//  HorizontalViewCell.swift
//  GoKerala
//
//  Created by Manu Aravind on 12/04/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class HorizontalViewCell: UICollectionViewCell {

    @IBOutlet weak var labelString: UILabel!
    
    
    @IBOutlet weak var bgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func updateCell(str: String){
        labelString.text = str
        
    }

}
