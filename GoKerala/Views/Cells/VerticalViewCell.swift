//
//  VerticalViewCell.swift
//  GoKerala
//
//  Created by Manu Aravind on 13/04/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class VerticalViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var typeImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        typeImageView.setImageColor(color: UIColor.init(hex: "8b8e96"))
    }

}
