//
//  UIImageView.swift
//  GoKerala
//
//  Created by Manu Aravind on 13/04/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//
import UIKit
extension UIImageView {
  func setImageColor(color: UIColor) {
    let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
    self.image = templateImage
    self.tintColor = color
  }
}
