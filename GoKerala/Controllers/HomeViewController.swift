//
//  HomeViewController.swift
//  GoKerala
//
//  Created by Manu Aravind on 12/04/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    let cellId = "HorizontalViewCell"
    let cellIdVertical = "VerticalViewCell"
    var districtsArray : [String] = ["Thiruvanamthapuram", "Kollam", "Pathanamthitta", "Alapuzha", "Kottayam","Idukki", "Ernakulam", "Thrissur", "Palakkad", "Malappuram","Wayanad", "Kozhikode", "Kannur", "Kazarkode"]
    var colorsArray : [UIColor] = [UIColor.init(hex: "294019"),  UIColor.init(hex: "5a9030"),  UIColor.init(hex: "5a9030"),  UIColor.init(hex: "5a9030"),  UIColor.init(hex: "5a9030"),  UIColor.init(hex: "5a9030"),  UIColor.init(hex: "5a9030"),  UIColor.init(hex: "5a9030"),  UIColor.init(hex: "5a9030"),  UIColor.init(hex: "5a9030"),  UIColor.init(hex: "5a9030"),  UIColor.init(hex: "5a9030"),  UIColor.init(hex: "5a9030"),  UIColor.init(hex: "5a9030")]
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    var positionIndex = 0
    var previousPosition = 0
    
    @IBOutlet weak var collectionViewList: UICollectionView!
    
    
    let cellItem_ID = "ItemTableViewCell"
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let screenSize = UIScreen.main.bounds.size
        let cellWidth = CGFloat(screenSize.width/2)
        let cellHeight = CGFloat(70.0)
        
        
        // horizontal
        let insetX = (view.bounds.width - cellWidth) / 2.0
        let layout = collectionView!.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        collectionView.register(UINib.init(nibName: cellId, bundle: nil), forCellWithReuseIdentifier: cellId)
        collectionView?.contentInset = UIEdgeInsets(top: 0, left: insetX, bottom: 0, right: insetX)
        
        //vertical
        collectionViewList.register(UINib.init(nibName: cellIdVertical, bundle: nil), forCellWithReuseIdentifier: cellIdVertical)
        collectionViewList.frame.size.width = cellWidth/2
        self.collectionViewList.delegate = self
        self.collectionViewList.dataSource = self
        
        
        
        
        self.tableView.register(UINib.init(nibName: cellItem_ID, bundle: nil), forCellReuseIdentifier: cellItem_ID)//(UINib.init(nibName: cellItem_ID, bundle: nil), forCellWithReuseIdentifier: cellItem_ID)
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionView {
            return colorsArray.count
        }
        return 25
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.collectionView {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! HorizontalViewCell
            cell.updateCell(str: districtsArray[indexPath.row])
            cell.bgView.backgroundColor = colorsArray[indexPath.row]
            return cell
        }
        
        let cell = collectionViewList.dequeueReusableCell(withReuseIdentifier: cellIdVertical, for: indexPath) as! VerticalViewCell
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collectionView {
            moveToCenter(position: indexPath.row)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if scrollView == self.collectionView {
            let layout = self.collectionView?.collectionViewLayout  as! UICollectionViewFlowLayout
            let cellWidthIncludingSpacing = layout.itemSize.width + layout.minimumLineSpacing
            var offset = targetContentOffset.pointee
            let index = (offset.x +  scrollView.contentInset.left) / cellWidthIncludingSpacing
            let roundedIndex = round(index)
            offset = CGPoint(x: roundedIndex * cellWidthIncludingSpacing - scrollView.contentInset.left, y: -scrollView.contentInset.top)
            targetContentOffset.pointee = offset
            positionIndex = Int(roundedIndex)
            moveToCenter(position: positionIndex)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.collectionView {
            moveToCenter(position: positionIndex)
        }
    }
    
    private func moveToCenter(position :  Int){
        self.collectionView.scrollToItem(at: IndexPath(row: Int(position), section: 0), at: .centeredHorizontally, animated: true)
        if(previousPosition != position){
            colorsArray[position] = UIColor.init(hex: "294019")
            colorsArray[previousPosition] = UIColor.init(hex: "5a9030")
        }
        
        previousPosition = position
        collectionView.reloadData()
    }
}


extension HomeViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  125
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellItem_ID, for: indexPath) as! ItemTableViewCell
        return cell
    }
    
    
}
